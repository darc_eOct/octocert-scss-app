import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AboutComponent } from './home/about/about.component';
import { DocumentationComponent } from './home/documentation/documentation.component';
import { HelpComponent } from './home/help/help.component';
import { ModalComponent } from './home/modal/modal.component';
import { InstitutionComponent } from './institution/institution.component';
import { AccountComponent } from './home/account/account.component';
import { AdminsComponent } from './institution/admins/admins.component';
import { CertificatesComponent } from './institution/certificates/certificates.component';
import { CoursesComponent } from './institution/courses/courses.component';
import { DepartmentsComponent } from './institution/departments/departments.component';
import { ProfessorsComponent } from './institution/professors/professors.component';
import { StudentsComponent } from './institution/students/students.component';
import { AdminComponent } from './admin/admin.component';
import { EmployerComponent } from './employer/employer.component';
import { StudentComponent } from './student/student.component';
import { HomenavComponent } from './home/homenav/homenav.component';


const appRoutes: Routes = [
  { path: 'home', component: HomeComponent },
  { path: 'home/about', component: AboutComponent },
  { path: 'home/documentation', component: DocumentationComponent },
  { path: 'home/help', component: HelpComponent },
  { path: 'institution', component: InstitutionComponent },
  { path: 'institution/certificates', component: CertificatesComponent},
  { path: 'institution/courses', component: CoursesComponent},
  { path: 'institution/departments', component: DepartmentsComponent},
  { path: 'institution/professors', component: ProfessorsComponent},
  { path: 'institution/students', component: StudentsComponent},
  { path: 'student', component: StudentComponent },
  { path: 'employer', component: EmployerComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' } 

];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    DocumentationComponent,
    HelpComponent,
    ModalComponent,
    InstitutionComponent,
    AccountComponent,
    AdminsComponent,
    CertificatesComponent,
    CoursesComponent,
    DepartmentsComponent,
    ProfessorsComponent,
    StudentsComponent,
    AdminComponent,
    EmployerComponent,
    StudentComponent,
    HomenavComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
