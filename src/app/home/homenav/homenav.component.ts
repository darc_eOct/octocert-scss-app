import { Component, OnInit } from '@angular/core';
import { RouterModule, Routes } from "@angular/router";
import { NgModule } from '@angular/core';

import { AppComponent } from '../../app.component';
import { HomeComponent } from '../home.component';
import { AboutComponent } from '../about/about.component';
import { DocumentationComponent } from '../documentation/documentation.component';
import { HelpComponent } from '../help/help.component';

const appRoutes: Routes = [
  { path: 'home/about', component: AboutComponent },
  { path: 'home/documentation', component: DocumentationComponent },
  { path: 'home/help', component: HelpComponent },
  { path: '', redirectTo: '/home', pathMatch: 'full' } 

];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    AboutComponent,
    DocumentationComponent,
    HelpComponent,
  ],
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  providers: [],
  bootstrap: [HomeComponent]
})

@Component({
  selector: 'app-homenav',
  templateUrl: './homenav.component.html',
  styleUrls: ['./homenav.component.scss']
})
export class HomenavComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
